#include "JetMonitoring/JetKinematicHistos.h"
#include "JetMonitoring/JetAttributeHisto.h"
#include "JetMonitoring/HistoDefinitionTool.h"
#include "JetMonitoring/HistosForJetSelection.h"
#include "JetMonitoring/LeadingJetsRelations.h"
#include "JetMonitoring/EfficiencyResponseHistos.h"
#include "JetMonitoring/HIEfficiencyResponseHistos.h"
#include "JetMonitoring/JetContainerHistoFiller.h"
#include "JetMonitoring/JetSelectorAttribute.h"
#include "JetMonitoring/HIJetUEMonitoring.h"

#include "../JetMonitoringTool.h"

DECLARE_COMPONENT( JetKinematicHistos )
DECLARE_COMPONENT( JetMonitoringTool )
DECLARE_COMPONENT( HistoDefinitionTool )
DECLARE_COMPONENT( HistosForJetSelection )
DECLARE_COMPONENT( LeadingJetsRelations )
DECLARE_COMPONENT( EfficiencyResponseHistos )
DECLARE_COMPONENT( HIEfficiencyResponseHistos )
DECLARE_COMPONENT( JetContainerHistoFiller )
DECLARE_COMPONENT( JetSelectorAttribute )
DECLARE_COMPONENT( HIJetUEMonitoring )

DECLARE_COMPONENT( JetAttributeHisto )





 

